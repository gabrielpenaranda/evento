<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware('auth')->prefix('admin')->group(function()
{
        Route::resource('usuarios', 'UsuariosController');
        Route::get('/', 'AdminController@index')->name('admin_principal');

        Route::prefix('estado')->group(function()
        {
            Route::get('/', 'EstadoController@index')->name('admin_estado');
            Route::get('/crear', 'EstadoController@create')->name('admin_estado_create');
            Route::post('/guardar', 'EstadoController@store')->name('admin_estado_store');
            Route::delete('/borrar/{estado}', 'EstadoController@delete')->name('admin_estado_delete');
            Route::get('/editar/{estado}', 'EstadoController@edit')->name('admin_estado_edit');
            Route::put('/actualizar/{estado}', 'EstadoController@update')->name('admin_estado_update');
        });

        Route::prefix('ciudad')->group(function()
        {
            Route::get('/', 'CiudadController@index')->name('admin_ciudad');
            Route::get('/crear', 'CiudadController@create')->name('admin_ciudad_create');
            Route::post('/guardar', 'CiudadController@store')->name('admin_ciudad_store');
            Route::delete('/borrar/{ciudad}', 'CiudadController@delete')->name('admin_ciudad_delete');
            Route::get('/editar/{ciudad}', 'CiudadController@edit')->name('admin_ciudad_edit');
            Route::put('/actualizar/{ciudad}', 'CiudadController@update')->name('admin_ciudad_update');
        });

        Route::prefix('lugar')->group(function()
        {
            Route::get('/', 'LugarController@index')->name('admin_lugar');
            Route::get('/crear', 'LugarController@create')->name('admin_lugar_create');
            Route::post('/guardar', 'LugarController@store')->name('admin_lugar_store');
            Route::delete('/borrar/{lugar}', 'LugarController@delete')->name('admin_lugar_delete');
            Route::get('/editar/{lugar}', 'LugarController@edit')->name('admin_lugar_edit');
            Route::put('/actualizar/{lugar}', 'LugarController@update')->name('admin_lugar_update');
        });

        Route::prefix('tipoevento')->group(function()
        {
            Route::get('/', 'TipoeventoController@index')->name('admin_tipoevento');
            Route::get('/crear', 'TipoeventoController@create')->name('admin_tipoevento_create');
            Route::post('/guardar', 'TipoeventoController@store')->name('admin_tipoevento_store');
            Route::delete('/borrar/{tipoevento}', 'TipoeventoController@delete')->name('admin_tipoevento_delete');
            Route::get('/editar/{tipoevento}', 'TipoeventoController@edit')->name('admin_tipoevento_edit');
            Route::put('/actualizar/{tipoevento}', 'TipoeventoController@update')->name('admin_tipoevento_update');
        });

        Route::prefix('evento')->group(function()
        {
            Route::get('/', 'EventoController@index')->name('admin_evento');
            Route::get('/crear', 'EventoController@create')->name('admin_evento_create');
            Route::post('/guardar', 'EventoController@store')->name('admin_evento_store');
            Route::delete('/borrar/{evento}', 'EventoController@delete')->name('admin_evento_delete');
            Route::get('/editar/{evento}', 'EventoController@edit')->name('admin_evento_edit');
            Route::put('/actualizar/{evento}', 'EventoController@update')->name('admin_evento_update');
        });

        Route::prefix('tipolugar')->group(function()
        {
            Route::get('/', 'TipolugarController@index')->name('admin_tipolugar');
            Route::get('/crear', 'TipolugarController@create')->name('admin_tipolugar_create');
            Route::post('/guardar', 'TipolugarController@store')->name('admin_tipolugar_store');
            Route::delete('/borrar/{tipolugar}', 'TipolugarController@delete')->name('admin_tipolugar_delete');
            Route::get('/editar/{tipolugar}', 'TipolugarController@edit')->name('admin_tipolugar_edit');
            Route::put('/actualizar/{tipolugar}', 'TipolugarController@update')->name('admin_tipolugar_update');
        });

        Route::prefix('tipocontacto')->group(function()
        {
            Route::get('/', 'TipocontactoController@index')->name('admin_tipocontacto');
            Route::get('/crear', 'TipocontactoController@create')->name('admin_tipocontacto_create');
            Route::post('/guardar', 'TipocontactoController@store')->name('admin_tipocontacto_store');
            Route::delete('/borrar/{tipocontacto}', 'TipocontactoController@delete')->name('admin_tipocontacto_delete');
            Route::get('/editar/{tipocontacto}', 'TipocontactoController@edit')->name('admin_tipocontacto_edit');
            Route::put('/actualizar/{tipocontacto}', 'TipocontactoController@update')->name('admin_tipocontacto_update');
        });

        Route::prefix('cliente')->group(function()
        {
            Route::get('/', 'ClienteController@index')->name('admin_cliente');
            Route::get('/crear', 'ClienteController@create')->name('admin_cliente_create');
            Route::post('/guardar', 'ClienteController@store')->name('admin_cliente_store');
            Route::delete('/borrar/{cliente}', 'ClienteController@delete')->name('admin_cliente_delete');
            Route::get('/editar/{cliente}', 'ClienteController@edit')->name('admin_cliente_edit');
            Route::put('/actualizar/{cliente}', 'ClienteController@update')->name('admin_cliente_update');
        });

        Route::prefix('publicidad')->group(function()
        {
            Route::get('/', 'PublicidadController@index')->name('admin_publicidad');
            Route::get('/crear', 'PublicidadController@create')->name('admin_publicidad_create');
            Route::post('/guardar', 'PublicidadController@store')->name('admin_publicidad_store');
            Route::delete('/borrar/{publicidad}', 'PublicidadController@delete')->name('admin_publicidad_delete');
            Route::get('/editar/{publicidad}', 'PublicidadController@edit')->name('admin_publicidad_edit');
            Route::put('/actualizar/{publicidad}', 'PublicidadController@update')->name('admin_publicidad_update');
            Route::get('/ver/{publicidad}', 'PublicidadController@view')->name('admin_publicidad_view');
        });

        Route::prefix('contacto')->group(function()
        {
            Route::get('/', 'TipocontactoController@index')->name('admin_contacto');
            Route::get('/crear', 'TipocontactoController@create')->name('admin_contacto_create');
            Route::post('/guardar', 'TipocontactoController@store')->name('admin_contacto_store');
            Route::delete('/borrar/{contacto}', 'ContactoController@delete')->name('admin_contacto_delete');
            Route::get('/editar/{contacto}', 'ContactoController@edit')->name('admin_tontacto_edit');
            Route::put('/actualizar/{contacto}', 'ContactoController@update')->name('admin_contacto_update');
        });
});

Route::get('evento/en/{id}', 'PrincipalController@eventos')->name('principal_eventos');
