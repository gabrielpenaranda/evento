<?php

namespace Evento;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function paises()
    {
        return $table->hasMany('Evento\Pais');
    }

    public function estados()
    {
        return $this->hasMany('Evento\Estado');
    }

    public function ciudades()
    {
        return $this->hasMany('Evento\Ciudad');
    }

    public function tipo_lugares()
    {
        return $this->hasMany('Evento\Tipo_lugar');
    }

    public function tipo_eventos()
    {
        return $this->hasMany('Evento\Tipo_evento');
    }

    public function lugares()
    {
        return $this->hasMany('Evento\Lugar');
    }

    public function eventos()
    {
        return $this->hasMany('Evento\Evento');
    }

    public function clientes()
    {
        return $this->hasMany('Evento\Cliente');
    }

    public function publicidades()
    {
        return $this->hasMany('Evento\Publicidad');
    }

    public function tipo_contactos()
    {
        return $this->hasMany('Evento\Tipo_contacto');
    }

    public function contactos()
    {
        return $this->hasMany('Evento\Contacto');
    }
}
