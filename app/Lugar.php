<?php

namespace Evento;

use Illuminate\Database\Eloquent\Model;

class Lugar extends Model
{
    protected $table = 'lugares';

    protected $fillable = ['nombre_lugar', 'descripcion_lugar', 'imagen_lugar', 'ciudad_id', 'tipo_lugar_id'];

    public function ciudad()
    {
        return $this->belongsTo('Evento\Ciudad');
    }

    public function tipo_lugar()
    {
        return $this->belongsTo('Evento\Tipo_lugar');
    }

    public function eventos()
    {
        return $this->hasMany('Evento\Evento');
    }

    public function user()
    {
        return $this->belongsTo('Evento\User');
    }
}
