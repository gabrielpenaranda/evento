<?php

namespace Evento;

use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    protected $table = 'eventos';

    protected $fillable = ['nombre_evento', 'descripcion_evento', 'fecha_evento', 'hora_evento', 'imagen_evento', 'lugar_id', 'tipo_evento_id'];

    public function lugar()
    {
        return $this->belongsTo('Evento\Lugar');
    }

    public function tipo_evento()
    {
        return $this->belongsTo('Evento\Tipo_evento');
    }

    public function user()
    {
        return $this->belongsTo('Evento\User');
    }
}
