<?php

namespace Evento;

use Illuminate\Database\Eloquent\Model;

class Tipo_contacto extends Model
{
    protected $table = 'tipo_contactos';

    protected $fillable = ['nombre_tipocontacto'];

    public function contactos()
    {
        retrun $this->hasMany('Evento\Contacto');
    }

    public function user()
    {
        return $this->belongsTo('Evento\User');
    }
}
