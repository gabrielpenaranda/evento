<?php

namespace Evento;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'clientes';

    protected $fillable = ['nombre_cliente', 'rif_cliente', 'direccion_cliente', 'ciudad_id'];

    public function publicidades()
    {
        return $this->hasMany('Evento\Publicidad');
    }

    public function ciudad()
    {
        return $this->belongsTo('Evento\Ciudad');
    }

    public function contactos()
    {
        return $this->hasMany('Evento\Contacto');
    }

    public function user()
    {
        return $this->belongsTo('Evento\User');
    }
}
