<?php

namespace Evento;

use Illuminate\Database\Eloquent\Model;

class Contacto extends Model
{
    protected $table = 'contactos';

    protected $fillable = ['descripcion_contacto', 'fecha_contacto'];

    public function cliente()
    {
        retrun $this->belongsTo('Evento\Cliente');
    }

    public function tipo_contacto()
    {
        retrun $this->belongsTo('Evento\Tipo_contacto');
    }

    public function user()
    {
        return $this->belongsTo('Evento\User');
    }
}
