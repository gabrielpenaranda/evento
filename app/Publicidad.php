<?php

namespace Evento;

use Illuminate\Database\Eloquent\Model;

class Publicidad extends Model
{
    protected $table = 'publicidades';

    protected $fillable = ['descripcion_publicidad', 'fecha_inicio_publicidad', 'fecha_fin_publicidad', 'imagen_publicidad', 'cliente_id'];

    public function cliente()
    {
        return $this->belongsTo('Evento\Cliente');
    }

    public function estados()
    {
        // return $this->belongsToMany('Evento\Estado', 'estado_publicidad', 'publicidad_id', 'estado_id');
        return $this->belongsToMany('Evento\Estado', 'estado_publicidad')->withPivot('estado_id', 'publicidad_id');
    }

    public function user()
    {
        return $this->belongsTo('Evento\User');
    }
}
