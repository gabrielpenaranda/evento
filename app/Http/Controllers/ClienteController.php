<?php

namespace Evento\Http\Controllers;

use Illuminate\Http\Request;
use Evento\Http\Requests\CreateClienteRequest;
use Evento\Http\Requests\UpdateClienteRequest;
use Evento\Cliente;
use Evento\Ciudad;

class ClienteController extends Controller
{
    public function index()
    {
        $cliente = Cliente::orderBy('nombre_cliente', 'asc')->paginate(10);
        return view('admin.cliente.index')->with(['cliente' => $cliente]);
    }

    public function create()
    {
        $cliente = new Cliente;
        $ciudad = Ciudad::all();
        return view('admin.cliente.create')->with(['cliente' => $cliente])->with(['ciudad' => $ciudad]);
    }

    public function store(CreateClienteRequest $request)
    {
        $cliente = new Cliente;
        $cliente->nombre_cliente = $request->get('nombre_cliente');
        $cliente->rif_cliente = $request->get('rif_cliente');
        $cliente->direccion_cliente = $request->get('direccion_cliente');
        $cliente->ciudad_id = $request->get('ciudad_id');
        $cliente->user_id = auth()->user()->id;
        $cliente->save();
        session()->flash('message', 'Cliente creado!');

        return redirect()->route('admin_cliente');
    }

    public function edit(Cliente $cliente)
    {
        if ($cliente->user_id != \Auth::user()->id)
        {
            return redirect()->route('admin_cliente');
            session()->flash('message', 'Usuario no válido!');
        }
        $ciudad = Ciudad::all();
        return view('admin.cliente.edit')->with(['cliente' => $cliente])->with(['ciudad' => $ciudad]);
    }

    public function update(Cliente $cliente, UpdateClienteRequest $request)
    {
        $cliente->update(
                $request->only('nombre_cliente', 'rif_cliente', 'direccion_cliente', 'ciudad_id')
            );
        session()->flash('message', 'Cliente actualizado!');

        return redirect()->route('admin_cliente', ['cliente' => $cliente->id]);
    }

    public function delete(Cliente $cliente)
    {
        if ($cliente->user_id != \Auth::user()->id)
        {
            return redirect()->route('admin_cliente');
        }

        $cliente->delete();

        session()->flash('message', 'Cliente eliminado!');

        return redirect()->route('admin_cliente');
    }
}
