<?php

namespace Evento\Http\Controllers;

use Illuminate\Http\Request;
use Evento\Http\Requests\CreateEventoRequest;
use Evento\Http\Requests\UpdateEventoRequest;
use Evento\Evento;
use Evento\Lugar;
use Evento\Tipo_evento;

class EventoController extends Controller
{
    public function index()
    {
        $evento = Evento::orderBy('fecha_evento', 'asc')->orderBy('hora_evento', 'asc')->paginate(10);
        return view('admin.evento.index')->with(['evento' => $evento]);
    }

    public function create()
    {
        $evento = new Evento;
        $lugar = Lugar::orderBy('nombre_lugar', 'asc')->get();
        $tipoevento = Tipo_evento::orderBy('descripcion_tipo_evento', 'asc')->get();
        return view('admin.evento.create')->with(['evento' => $evento])->with(['lugar' => $lugar])->with(['tipoevento' => $tipoevento]);
    }

    public function store(CreateEventoRequest $request)
    {
        $evento = new Evento;
        $evento->nombre_evento = $request->get('nombre_evento');
        $evento->lugar_id = $request->get('lugar_id');
        $evento->descripcion_evento = $request->get('descripcion_evento');
        $evento->tipo_evento_id = $request->get('tipo_evento_id');
        // $fecha = new \DateTime($request->get('fecha_evento'));
        // $f=$fecha->format('d/m/Y');
        $evento->fecha_evento = $request->get('fecha_evento');
        // $evento->fecha_evento = $f;
        $evento->hora_evento = $request->get('hora_evento');
        $evento->imagen_evento = ' ';
        $evento->user_id = auth()->user()->id;
        $evento->save();
        session()->flash('message', 'Evento creado!');

        return redirect()->route('admin_evento');
    }

    public function edit(Evento $evento)
    {
        if ($evento->user_id != \Auth::user()->id)
        {
            return redirect()->route('admin_evento');
            session()->flash('message', 'Usuario no válido!');
        }
        $lugar = Lugar::all();
        $tipoevento = Tipo_evento::all();
        return view('admin.evento.edit')->with(['evento' => $evento])->with(['lugar' => $lugar])->with(['tipoevento' => $tipoevento]);
    }

    public function update(Evento $evento, UpdateEventoRequest $request)
    {
        $evento->update(
                $request->only('nombre_evento', 'descripcion_evento', 'lugar_id', 'ciudad_id', 'fecha_evento', 'hora_evento')
            );
        session()->flash('message', 'Evento actualizado!');

        return redirect()->route('admin_evento', ['evento' => $evento->id]);
    }

    public function delete(Evento $evento)
    {
        if ($evento->user_id != \Auth::user()->id)
        {
            return redirect()->route('admin_evento');
        }

        $evento->delete();

        session()->flash('message', 'Evento eliminado!');

        return redirect()->route('admin_evento');
    }
}
