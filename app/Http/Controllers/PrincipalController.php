<?php

namespace Evento\Http\Controllers;

use Illuminate\Http\Request;

use Evento\Estado;

class PrincipalController extends Controller
{
    public function index()
    {
        $menu = Estado::orderBy('nombre_estado'. 'asc')->orderBy('nombre_ciudad', 'asc')->get();
        return view('front.index', ['menu' => $menu]);
    }
}
