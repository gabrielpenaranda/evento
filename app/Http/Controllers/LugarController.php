<?php

namespace Evento\Http\Controllers;

use Illuminate\Http\Request;
use Evento\Http\Requests\CreateLugarRequest;
use Evento\Http\Requests\UpdateLugarRequest;
use Evento\Lugar;
use Evento\Ciudad;
use Evento\Tipo_lugar;

class LugarController extends Controller
{
    public function index()
    {
        $lugar = Lugar::orderBy('nombre_lugar', 'asc')->paginate(10);
        return view('admin.lugar.index')->with(['lugar' => $lugar]);
    }

    public function create()
    {
        $lugar = new Lugar;
        $ciudad = Ciudad::all();
        $tipolugar = Tipo_lugar::all();
        return view('admin.lugar.create')->with(['lugar' => $lugar])->with(['ciudad' => $ciudad])->with(['tipolugar' => $tipolugar]);
    }

    public function store(CreateLugarRequest $request)
    {
        $lugar = new Lugar;
        $lugar->nombre_lugar = $request->get('nombre_lugar');
        $lugar->tipo_lugar_id = $request->get('tipo_lugar_id');
        $lugar->descripcion_lugar = $request->get('descripcion_lugar');
        $lugar->direccion_lugar = $request->get('direccion_lugar');
        $lugar->imagen_lugar = ' ';
        $lugar->ciudad_id = $request->get('ciudad_id');
        $lugar->user_id = auth()->user()->id;
        $lugar->save();
        session()->flash('message', 'Lugar creado!');

        return redirect()->route('admin_lugar');
    }

    public function edit(Lugar $lugar)
    {
        if ($lugar->user_id != \Auth::user()->id)
        {
            return redirect()->route('admin_lugar');
            session()->flash('message', 'Usuario no válido!');
        }
        $ciudad = Ciudad::all();
        $tipolugar = Tipo_lugar::all();
        return view('admin.lugar.edit')->with(['lugar' => $lugar])->with(['ciudad' => $ciudad])->with(['tipolugar' => $tipolugar]);
    }

    public function update(Lugar $lugar, UpdateLugarRequest $request)
    {
        $lugar->update(
                $request->only('nombre_lugar', 'tipo_lugar_id', 'descripcion_lugar', 'direccion_lugar', 'ciudad_id')
            );
        session()->flash('message', 'Lugar actualizado!');

        return redirect()->route('admin_lugar', ['lugar' => $lugar->id]);
    }

    public function delete(Lugar $lugar)
    {
        if ($lugar->user_id != \Auth::user()->id)
        {
            return redirect()->route('admin_lugar');
        }

        $lugar->delete();

        session()->flash('message', 'Lugar eliminado!');

        return redirect()->route('admin_lugar');
    }
}
