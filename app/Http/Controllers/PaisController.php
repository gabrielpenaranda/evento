<?php

namespace Evento\Http\Controllers;

use Illuminate\Http\Request;

use Evento\Http\Requests\CreatePaisRequest;
use Evento\Http\Requests\UpdatePaisRequest;
use Evento\Pais;

class PaisController extends Controller
{
    public function index()
    {
        $pais = Pais::orderBy('nombre_pais', 'asc')->paginate(10);
        return view('admin.pais.index')->with(['pais' => $pais]);
    }

    public function create()
    {
        $pais = new Pais;
        return view('admin.pais.create')->with(['pais' => $pais]);
    }

    public function store(CreatePaisRequest $request)
    {
        $pais = new Pais;
        $pais->nombre_pais = $request->get('nombre_pais');
        $pais->user_id = auth()->user()->id;
        $pais->save();
        session()->flash('message', 'País creado!');

        return redirect()->route('admin_pais');
    }

    public function edit(Pais $pais)
    {
        if ($pais->user_id != \Auth::user()->id)
        {
            return redirect()->route('admin_pais');
            session()->flash('message', 'Usuario no válido!');
        }

        return view('admin.pais.edit')->with(['pais' => $pais]);
    }

    public function update(Pais $pais, UpdatePaisRequest $request)
    {
        $pais->update(
                $request->only('nombre_pais')
            );
        session()->flash('message', 'País actualizado!');

        return redirect()->route('admin_pais', ['pais' => $pais->id]);
    }

    public function delete(Pais $pais)
    {
        if ($pais->user_id != \Auth::user()->id)
        {
            return redirect()->route('admin_pais');
        }

        $pais->delete();

        session()->flash('message', 'País eliminado!');

        return redirect()->route('admin_pais');
    }

}
