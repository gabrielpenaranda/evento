<?php

namespace Evento\Http\Controllers;

use Illuminate\Http\Request;

use Evento\Http\Requests\CreateTipolugarRequest;
use Evento\Http\Requests\UpdateTipolugarRequest;
use Evento\Tipo_lugar;

class TipolugarController extends Controller
{
    public function index()
    {
        $tipolugar = Tipo_lugar::orderBy('descripcion_tipo_lugar', 'asc')->paginate(10);
        return view('admin.tipolugar.index')->with(['tipolugar' => $tipolugar]);
    }

    public function create()
    {
        $tipolugar = new Tipo_lugar;
        return view('admin.tipolugar.create')->with(['tipolugar' => $tipolugar]);
    }

    public function store(CreateTipolugarRequest $request)
    {
        $tipolugar = new Tipo_lugar;
        $tipolugar->descripcion_tipo_lugar = $request->get('descripcion_tipo_lugar');
        $tipolugar->user_id = auth()->user()->id;
        $tipolugar->save();
        session()->flash('message', 'Tipo lugar creado!');

        return redirect()->route('admin_tipolugar');
    }

    public function edit(Tipo_lugar $tipolugar)
    {
        if ($tipolugar->user_id != \Auth::user()->id)
        {
            return redirect()->route('admin_tipolugar');
            session()->flash('message', 'Usuario no válido!');
        }

        return view('admin.tipolugar.edit')->with(['tipolugar' => $tipolugar]);
    }

    public function update(Tipo_lugar $tipolugar, UpdateTipolugarRequest $request)
    {
        $tipolugar->update(
                $request->only('descripcion_tipo_lugar')
            );
        session()->flash('message', 'Tipo lugar actualizado!');

        return redirect()->route('admin_tipolugar', ['tipolugar' => $tipolugar->id]);
    }

    public function delete(Tipo_lugar $tipolugar)
    {
        if ($tipolugar->user_id != \Auth::user()->id)
        {
            return redirect()->route('admin_tipolugar');
        }

        $tipolugar->delete();

        session()->flash('message', 'Tipo lugar eliminado!');

        return redirect()->route('admin_tipolugar');
    }
}
