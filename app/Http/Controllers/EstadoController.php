<?php

namespace Evento\Http\Controllers;

use Illuminate\Http\Request;

use Evento\Http\Requests\CreateEstadoRequest;
use Evento\Http\Requests\UpdateEstadoRequest;
use Evento\Estado;

class EstadoController extends Controller
{
    public function index()
    {
        $estado = Estado::orderBy('nombre_estado', 'asc')->paginate(10);
        return view('admin.estado.index')->with(['estado' => $estado]);
    }

    public function create()
    {
        $estado = new Estado;
        return view('admin.estado.create')->with(['estado' => $estado]);
    }

    public function store(CreateEstadoRequest $request)
    {
        $estado = new Estado;
        $estado->nombre_estado = $request->get('nombre_estado');
        $estado->user_id = auth()->user()->id;
        $estado->save();
        session()->flash('message', 'Estado creado!');

        return redirect()->route('admin_estado');
    }

    public function edit(Estado $estado)
    {
        if ($estado->user_id != \Auth::user()->id)
        {
            return redirect()->route('admin_estado');
            session()->flash('message', 'Usuario no válido!');
        }
        return view('admin.estado.edit')->with(['estado' => $estado]);
    }

    public function update(Estado $estado, UpdateEstadoRequest $request)
    {
        $estado->update(
                $request->only('nombre_estado')
            );
        session()->flash('message', 'Estado actualizado!');

        return redirect()->route('admin_estado');
    }

    public function delete(Estado $estado)
    {
        if ($estado->user_id != \Auth::user()->id)
        {
            return redirect()->route('admin_estado');
        }

        $estado->delete();

        session()->flash('message', 'Estado eliminado!');

        return redirect()->route('admin_estado');
    }
}
