<?php

namespace Evento\Http\Controllers;

use Illuminate\Http\Request;

use Evento\Http\Requests\CreateCiudadRequest;
use Evento\Http\Requests\UpdateCiudadRequest;

use Evento\Ciudad;
use Evento\Estado;

class CiudadController extends Controller
{
    public function index()
    {
        $ciudad = Ciudad::orderBy('nombre_ciudad', 'asc')->paginate(10);
        $estado = Estado::all();
        return view('admin.ciudad.index')->with(['ciudad' => $ciudad])->with(['estado' => $estado]);
    }

    public function create()
    {
        $ciudad = new Ciudad;
        $estado = Estado::orderBy('nombre_estado', 'asc')->get();
        return view('admin.ciudad.create')->with(['ciudad' => $ciudad])->with(['estado' => $estado]);
    }

    public function store(CreateCiudadRequest $request)
    {
        $ciudad = new Ciudad;
        $ciudad->nombre_ciudad = $request->get('nombre_ciudad');
        $ciudad->estado_id = $request->get('estado_id');
        $ciudad->user_id = auth()->user()->id;
        $ciudad->save();
        session()->flash('message', 'Ciudad creada!');

        return redirect()->route('admin_ciudad');
    }

    public function edit(Ciudad $ciudad)
    {
        if ($ciudad->user_id != \Auth::user()->id)
        {
            return redirect()->route('admin_ciudad');
            session()->flash('message', 'Usuario no válido!');
        }
        $estado = Estado::all();
        return view('admin.ciudad.edit')->with(['ciudad' => $ciudad])->with(['estado' => $estado]);
    }

    public function update(Ciudad $ciudad, UpdateCiudadRequest $request)
    {
        $ciudad->update(
                $request->only('nombre_ciudad', 'estado_id')
            );
        session()->flash('message', 'Ciudad actualizada!');

        return redirect()->route('admin_ciudad');
    }

    public function delete(Ciudad $ciudad)
    {
        if ($ciudad->user_id != \Auth::user()->id)
        {
            return redirect()->route('admin_ciudad');
        }

        $ciudad->delete();

        session()->flash('message', 'Ciudad eliminada!');

        return redirect()->route('admin_ciudad');
    }
}
