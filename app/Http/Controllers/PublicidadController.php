<?php

namespace Evento\Http\Controllers;

use Illuminate\Http\Request;
use Evento\Http\Requests\CreatePublicidadRequest;
use Evento\Http\Requests\UpdatePublicidadRequest;
use Evento\Publicidad;
use Evento\Cliente;
use Evento\Estado;

class PublicidadController extends Controller
{
    public function index()
    {
        $publicidad = Publicidad::orderBy('fecha_inicio_publicidad', 'asc')->orderBy('fecha_final_publicidad', 'asc')->paginate(10);
        return view('admin.publicidad.index')->with(['publicidad' => $publicidad]);
    }

    public function create()
    {
        $publicidad = new Publicidad;
        $cliente = Cliente::all();
        $estado = Estado::all();
        $pestado = 0;
        return view('admin.publicidad.create')->with(['publicidad' => $publicidad])->with(['cliente' => $cliente])->with(['estado' => $estado])->with(['pestado' => $pestado]);
    }

    public function store(CreatePublicidadRequest $request)
    {
        $publicidad = new Publicidad;
        $publicidad->descripcion_publicidad = $request->get('descripcion_publicidad');
        $publicidad->fecha_inicio_publicidad = $request->get('fecha_inicio_publicidad');
        $publicidad->fecha_fin_publicidad = $request->get('fecha_fin_publicidad');
        $publicidad->imagen_publicidad = ' ';
        $publicidad->cliente_id = $request->get('cliente_id');
        $publicidad->user_id = auth()->user()->id;
        $publicidad->save();
        $publicidad->estados()->sync($request->estado_id);
        session()->flash('message', 'Publicidad creada!');

        return redirect()->route('admin_publicidad');
    }

    public function edit(Publicidad $publicidad)
    {
        if ($publicidad->user_id != \Auth::user()->id)
        {
            return redirect()->route('admin_publicidad');
            session()->flash('message', 'Usuario no válido!');
        }
        $cliente = Cliente::all();
        $estado = Estado::all();
        foreach ($publicidad->estados as $pe) {
            $pestado[] = $pe->pivot->estado_id;
        }
        // $pestado = $publicidad->estados->pivot->estados_id;
        // dd($pestado);
        return view('admin.publicidad.edit')->with(['publicidad' => $publicidad])->with(['cliente' => $cliente])->with(['estado' => $estado])->with(['pestado' => $pestado]);
    }

    public function update(Publicidad $publicidad, UpdatePublicidadRequest $request)
    {
        $publicidad->update(
                $request->only('descripcion_publicidad', 'fecha_inicio_publicidad', 'fecha_fin_publicidad', 'cliente_id')
            );
        $publicidad->estados()->sync($request->estado_id);
        session()->flash('message', 'Publicidad actualizada!');

        return redirect()->route('admin_publicidad', ['publicidad' => $publicidad->id]);
    }

    public function delete(Publicidad $publicidad)
    {
        if ($publicidad->user_id != \Auth::user()->id)
        {
            return redirect()->route('admin_publicidad');
        }
        $publicidad->estados()->detach($publicidad->id);
        $publicidad->delete();

        session()->flash('message', 'Publicidad eliminada!');

        return redirect()->route('admin_publicidad');
    }

    public function view(Publicidad $publicidad)
    {
        //
    }
}
