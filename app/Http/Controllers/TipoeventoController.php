<?php

namespace Evento\Http\Controllers;

use Illuminate\Http\Request;

use Evento\Http\Requests\CreateTipoeventoRequest;
use Evento\Http\Requests\UpdateTipoeventoRequest;
use Evento\Tipo_evento;

class TipoeventoController extends Controller
{
    public function index()
    {
        $tipoevento = Tipo_evento::orderBy('descripcion_tipo_lugar', 'asc')->paginate(10);
        return view('admin.tipoevento.index')->with(['tipoevento' => $tipoevento]);
    }

    public function create()
    {
        $tipoevento = new Tipo_evento;
        return view('admin.tipoevento.create')->with(['tipoevento' => $tipoevento]);
    }

    public function store(CreateTipoeventoRequest $request)
    {
        $tipoevento = new Tipo_evento;
        $tipoevento->descripcion_tipo_evento = $request->get('descripcion_tipo_evento');
        $tipoevento->user_id = auth()->user()->id;
        $tipoevento->save();
        session()->flash('message', 'Tipo evento creado!');

        return redirect()->route('admin_tipoevento');
    }

    public function edit(Tipo_evento $tipoevento)
    {
        if ($tipoevento->user_id != \Auth::user()->id)
        {
            return redirect()->route('admin_tipoevento');
            session()->flash('message', 'Usuario no válido!');
        }

        return view('admin.tipoevento.edit')->with(['tipoevento' => $tipoevento]);
    }

    public function update(Tipo_evento $tipoevento, UpdateTipoeventoRequest $request)
    {
        $tipoevento->update(
                $request->only('descripcion_tipo_evento')
            );
        session()->flash('message', 'Tipo evento actualizado!');

        return redirect()->route('admin_tipoevento', ['tipoevento' => $tipoevento->id]);
    }

    public function delete(Tipo_evento $tipoevento)
    {
        if ($tipoevento->user_id != \Auth::user()->id)
        {
            return redirect()->route('admin_tipoevento');
        }

        $tipoevento->delete();

        session()->flash('message', 'Tipo evento eliminado!');

        return redirect()->route('admin_tipoevento');
    }
}
