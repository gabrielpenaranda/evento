<?php

namespace Evento\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateEstadoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (\Auth::user())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre_estado' => 'required|unique:estados,nombre_estado'
        ];
    }
}
