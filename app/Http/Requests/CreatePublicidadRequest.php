<?php

namespace Evento\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreatePublicidadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (\Auth::user())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'descripcion_publicidad' => 'required|max:250',
            'fecha_inicio_publicidad' => 'required',
            'fecha_fin_publicidad' => 'required',
            'cliente_id' => 'required',
            'estado_id' => 'required'
        ];
    }
}
