<?php

namespace Evento\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateClienteRequest extends CreateClienteRequest
{
     /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre_cliente' => "required",
            'rif_cliente' => "required|unique:clientes,rif_cliente",
            'direccion_cliente' => "required"
        ];
    }
}
