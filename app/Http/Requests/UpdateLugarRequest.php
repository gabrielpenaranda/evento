<?php

namespace Evento\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateLugarRequest extends CreateLugarRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre_lugar' => "required",
            'descripcion_lugar' => "required",
            'direccion_lugar' => "required"
        ];
    }
}
