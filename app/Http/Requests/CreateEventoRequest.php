<?php

namespace Evento\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Input;

class CreateEventoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (\Auth::user())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $f=getdate();
        $hoy=$f['mday'].'/'.$f['mon'].'/'.$f['year'];
        return [
            'nombre_evento' => "required",
            'descripcion_evento' => "required",
            'fecha_evento' => "required|date_format:d/m/Y|after:".$hoy,
            'hora_evento' => "required"
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nombre_evento.required' => 'El nombre del evento es requerido',
            'descripcion_evento.required' => 'Debe agregar la descripcion del evento',
            'fecha_evento.required'  => 'Debe señalar la fecha del evento',
            'fecha_evento.after' => 'La fecha del evento debe ser posterior a la fecha de hoy',
            'hora_evento.required' => 'Debe señalar la hora del evento'
        ];
    }
}
