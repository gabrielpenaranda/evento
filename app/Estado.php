<?php

namespace Evento;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    protected $table = 'estados';

    protected $fillable = ['nombre_estado', 'pais_id'];

    public function ciudades()
    {
        return $this->hasMany('Evento\Ciudad');
    }

    public function publicidades()
    {
        return $this->belongsToMany('Evento\Publicidad', 'estado_publicidad', 'estado_id', 'publicidad_id');
    }

    public function user()
    {
        return $this->belongsTo('Evento\User');
    }
}
