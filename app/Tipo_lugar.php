<?php

namespace Evento;

use Illuminate\Database\Eloquent\Model;

class Tipo_lugar extends Model
{
    protected $table = 'tipo_lugares';

    protected $fillable = ['descripcion_tipo_lugar'];

    public function tipo_lugares()
    {
        return $this->hasMany('Evento\Lugar');
    }

    public function user()
    {
        return $this->belongsTo('Evento\User');
    }
}
