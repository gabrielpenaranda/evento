<?php

namespace Evento;

use Illuminate\Database\Eloquent\Model;

class Ciudad extends Model
{
    protected $table = 'ciudades';

    protected $fillable = ['nombre_ciudad', 'estado_id'];


    public function clientes()
    {
        return $this->hasMany('Evento\Cliente');
    }

    public function estado()
    {
        return $this->belongsTo('Evento\Estado');
    }

    public function user()
    {
        return $this->belongsTo('Evento\User');
    }
}
