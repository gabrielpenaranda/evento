<?php

namespace Evento;

use Illuminate\Database\Eloquent\Model;

class Tipo_evento extends Model
{
    protected $table = 'tipo_eventos';

    protected $fillable = ['descripcion_tipo_evento'];

    public function eventos()
    {
        return $this->hasMany('Evento\Evento');
    }

    public function user()
    {
        return $this->belongsTo('Evento\User');
    }
}
