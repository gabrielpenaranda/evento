@extends('admin.index')

@section('title', 'Editar Ciudad')

@section('stylesheets')
    @parent
@endsection

@section('content')
    @include('admin.ciudad._form', ['ciudad' => $ciudad, 'estado' => $estado])
@endsection

@section('javascripts')
    @parent
@endsection
