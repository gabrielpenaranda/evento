{{-- ESTADO --}}
<div class="container">
    <div class="row">
        <div class="col-xs-offset-2 col-xs-8">
            @if ($ciudad->exists)
                <h4>Edición de Ciudad</h4>
                <form action="{{ route('admin_ciudad_update', ['ciudad' => $ciudad->id]) }}" method="POST">
                {{ method_field('PUT') }}
            @else
                <h4>Nuevo Ciudad</h4>
                <form action="{{ route('admin_ciudad_store') }}" method="POST">
            @endif

            {{ csrf_field() }}

            <div class="form-group">
                <label for="nombre_ciudad">Ciudad:</label>
                <input type="text" name="nombre_ciudad" class="form-control" value="{{ $ciudad->nombre_ciudad or old('nombre_ciudad')}}" />
            </div>
            <div class="form-group">
                <label for="estado_id">Estado: </label>
                <select name="estado_id">
                    @foreach ($estado as $e)
                        @if ($ciudad->estado_id == $e->id)
                            <option value="{{ $e->id }}" selected>
                        @else
                            <option value="{{ $e->id }}">
                        @endif
                            {{ $e->nombre_estado }}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-success">Grabar</button>
                <a class="btn btn-danger waves-effect waves-light" href="{{ route('admin_ciudad') }}">Regresar</a>
            </div>

            </form>
        </div>
    </div>
</div>
