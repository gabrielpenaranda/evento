{{-- CIUDAD --}}

@extends('admin.index')

@section('stylesheets')
    @parent
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-offset-1 col-xs-8 text-center">
                <h4>
                    {{ 'Registro de Ciudad' }}
                </h4>
            </div>
            <div class="col-xs-2">
                <a class="btn btn-primary waves-effect waves-light" href="{{ route('admin_ciudad_create') }}">Crear Ciudad</a>
            </div>
            <div class="col-xs-offset-1 col-xs-10">
                @if ($ciudad != NULL)
                    <table class="table table-striped">
                        <thead>
                            <th class="text-left">Ciudad</th>
                            <th class="text-left">Estado</th>
                            <th class="text-center">Acciones</th>
                        </thead>
                        <tbody>
                            @foreach ($ciudad as $c)
                                <tr>
                                    <td class="text-left">
                                        {{ $c->nombre_ciudad }}
                                    </td>
                                    <td class="text-left">
                                        {{ $c->estado->nombre_estado }}
                                    </td>
                                    <td class="text-center">
                                        @if (Auth::check() && ($c->user_id == Auth::user()->id || $c->user->level == "administrador"))
                                            <form action="{{ route('admin_ciudad_delete', ['ciudad' => $c->id]) }}" method='POST'>
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button class="btn btn-danger waves-effect waves-light" title="Eliminar"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                                                <a class="btn btn-info waves-effect waves-light" href="{{ route('admin_ciudad_edit', ['ciudad' => $c->id]) }}" title="Editar"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                {!! $ciudad->render() !!}
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

  @section('javascripts')
      @parent
  @endsection
