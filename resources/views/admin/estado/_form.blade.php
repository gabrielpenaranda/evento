{{-- ESTADO --}}
<div class="container">
    <div class="row">
        <div class="col-xs-offset-2 col-xs-8">
            @if ($estado->exists)
                <h4>Edición de Estado</h4>
                <form action="{{ route('admin_estado_update', ['estado' => $estado->id]) }}" method="POST">
                {{ method_field('PUT') }}
            @else
                <h4>Nuevo Estado</h4>
                <form action="{{ route('admin_estado_store') }}" method="POST">
            @endif

            {{ csrf_field() }}

            <div class="form-group">
                <label for="nombre_pais">Estado:</label>
                <input type="text" name="nombre_estado" class="form-control" value="{{ $estado->nombre_estado or old('nombre_estado')}}" />
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success">Grabar</button>
                <a class="btn btn-danger waves-effect waves-light" href="{{ route('admin_estado') }}">Regresar</a>
            </div>

            </form>
        </div>
    </div>
</div>
