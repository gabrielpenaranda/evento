@extends('admin.index')

@section('title', 'Crear Estado')

@section('stylesheets')
    @parent
@endsection

@section('content')
    @include('admin.estado._form', ['estado' => $estado])
@endsection

@section('javascripts')
    @parent
@endsection
