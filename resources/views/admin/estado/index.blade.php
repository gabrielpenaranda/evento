{{-- ESTADO --}}

@extends('admin.index')

@section('stylesheets')
    @parent
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-offset-1 col-xs-8 text-center">
                <h4>
                    {{ 'Registro de Estado' }}
                </h4>
            </div>
            <div class="col-xs-2">
                <a class="btn btn-primary waves-effect waves-light" href="{{ route('admin_estado_create') }}">Crear Estado</a>
            </div>
            <div class="col-xs-offset-1 col-xs-10">
                @if ($estado != NULL)
                    <table class="table table-striped">
                        <thead>
                            <th class="text-left">Estado</th>
                            <th class="text-center">Acciones</th>
                        </thead>
                        <tbody>
                            @foreach ($estado as $e)
                                <tr>
                                    <td class="text-left">
                                        {{ $e->nombre_estado }}
                                    </td>
                                    <td class="text-center">
                                        @if (Auth::check() && ($e->user_id == Auth::user()->id || $e->user->level == "administrador"))
                                            <form action="{{ route('admin_estado_delete', ['estado' => $e->id]) }}" method='POST'>
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button class="btn btn-danger waves-effect waves-light" title="Eliminar"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                                                <a class="btn btn-info waves-effect waves-light" href="{{ route('admin_estado_edit', ['estado' => $e->id]) }}" title="Editar"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                {!! $estado->render() !!}
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

  @section('javascripts')
      @parent
  @endsection
