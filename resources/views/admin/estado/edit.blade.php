@extends('admin.index')

@section('title', 'Editar Estado')

@section('stylesheets')
    @parent
@endsection

@section('content')
    @include('admin.estado._form', ['estado' => $estado])
@endsection

@section('javascripts')
    @parent
@endsection
