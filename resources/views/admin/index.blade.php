{{-- INDEX ADMIN --}}

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    @section('stylesheets')
        <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('vendor/mdb/css/mdb.min.css') }}" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Chewy" rel="stylesheet">
        <link href="{{ asset('css/pw.css') }} rel="stylesheet"">
        {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
    @show

</head>
<body>
    <div id="app">

        <nav class="navbar navbar-static-top teal">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand waves-effect waves-light pw-logo" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}</a>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" role="button" aria-expanded="false">Tablas <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ route('admin_ciudad') }}">Ciudad</a></li>
                                <li><a href="{{ route('admin_estado') }}">Estado</a></li>
                                <li class="divider teal"></li>
                                <li><a href="{{ route('admin_tipolugar') }}">Tipo de Lugar</a></li>
                                <li><a href="{{ route('admin_tipoevento') }}">Tipo de Evento</a></li>
                                <li class="divider teal"></li>
                                <li><a href="{{ route('admin_cliente') }}">Clientes</a></li>
                            </ul>
                            <li><a href="{{ route('admin_lugar') }}">Lugar</a></li>
                            <li><a href="{{ route('admin_evento') }}">Evento</a></li>
                            <li><a href="{{ route('admin_publicidad') }}">Publicidad</a></li>
                            <li><a href='#'>Usuarios</a></li>
                            <li>
                                <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                Salir
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        @include('layouts._errors')
        @include('layouts._message')

        @yield('content')
    </div>

    <!-- Scripts -->
    @section('javascripts')
        <script src="{{ asset('vendor/jquery/jquery-3.2.1.min.js') }}"></script>
        <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('vendor/mdb/js/mdb.min.js') }}"></script>
        {{-- <script src="{{ asset('js/app.js') }}"></script> --}}
    @show
</body>
</html>
