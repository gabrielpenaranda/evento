{{-- EVENTO --}}

@extends('admin.index')

@section('stylesheets')
    @parent
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-10 text-center">
                <h4>
                    {{ 'Registro de Eventos' }}
                </h4>
            </div>
            <div class="col-xs-2">
                <a class="btn btn-primary waves-effect waves-light" href="{{ route('admin_evento_create') }}">Crear Evento</a>
            </div>
            <div class="col-xs-12">
                @if ($evento != NULL)
                    <table class="table table-striped">
                        <thead>
                            <th class="text-left">Fecha/Hora</th>
                            <th class="text-left">Evento</th>
                            {{-- <th class="text-left">Descripción</th> --}}
                            <th class="text-left">Tipo</th>
                            <th class="text-left">Lugar</th>
                            <th class="text-left">Ubicación</th>
                            <th class="text-center">Acciones</th>
                        </thead>
                        <tbody>
                            @foreach ($evento as $e)
                                {{-- @php
                                    $fecha = new DateTime($e->fecha_evento);
                                    $fecha->format('d/m/Y')
                                @endphp --}}
                                <tr>
                                    <td class="text-left" id='fecha_evento'>
                                        {{ $e->fecha_evento.' '.$e->hora_evento }}
                                    </td>
                                    <td class="text-left">
                                        {{ $e->nombre_evento }}
                                    </td>
                                    {{-- <td class="text-left">
                                        {{ $e->descripcion_evento }}
                                    </td> --}}
                                    <td class="text-left">
                                        {{ $e->tipo_evento->descripcion_tipo_evento }}
                                    </td>
                                    <td class="text-left">
                                        {{ $e->lugar->nombre_lugar }}
                                    </td>
                                    <td class="text-left">
                                        {{ $e->lugar->ciudad->nombre_ciudad.', '.$e->lugar->ciudad->estado->nombre_estado }}
                                    </td>
                                    <td class="text-center">
                                        @if (Auth::check() && ($e->user_id == Auth::user()->id || $e->user->level == "administrador"))
                                            <form action="{{ route('admin_evento_delete', ['evento' => $e->id]) }}" method='POST'>
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button class="btn btn-danger waves-effect waves-light" title="Eliminar"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                                                <a class="btn btn-info waves-effect waves-light" href="{{ route('admin_evento_edit', ['evento' => $e->id]) }}" title="Editar"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                {!! $evento->render() !!}
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

  @section('javascripts')
      @parent
  @endsection
