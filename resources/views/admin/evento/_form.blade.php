{{-- ESTADO --}}
<div class="container">
    <div class="row">
        <div class="col-xs-offset-2 col-xs-6">
            @if ($evento->exists)
                <h4>Edición de Evento</h4>
                <form action="{{ route('admin_evento_update', ['evento' => $evento->id]) }}" method="POST">
                {{ method_field('PUT') }}
            @else
                <h4>Nuevo Evento</h4>
                <form action="{{ route('admin_evento_store') }}" method="POST">
            @endif
        </div>
        <div class="col-xs-2">
            <a class="btn btn-danger waves-effect waves-light" href="{{ route('admin_evento') }}">Regresar</a>
        </div>
        <div class="col-xs-offset-2 col-xs-8">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="nombre_evento">Nombre Evento:</label>
                <input type="text" name="nombre_evento" class="form-control" value="{{ $evento->nombre_evento or old('nombre_evento')}}" />
            </div>
            <div class="form-group">
                <label for="descripcion_evento">Descripción:</label>
                <textarea rows="10" name="descripcion_evento" class="form-control ckeditor" id="descripcion_evento">{{ $evento->descripcion_evento or old('descripcion_evento') }}</textarea>
            </div>
            {{-- @php
                $fecha = new DateTime($evento->fecha_evento);
                $f = $fecha->format('d-m-Y');
            @endphp --}}
            <div class="form-group">
                <label for="fecha_evento">Fecha:</label>
                <input type="date" name="fecha_evento" id="datepicker" class="form-control" value="{{ $evento->fecha_evento or old('fecha_evento')}}" />
            </div>
            <div class="form-group">
                <label for="hora_evento">Hora:</label>
                <input type="time" name="hora_evento" class="form-control timepicker" value="{{ $evento->hora_evento or old('hora_evento')}}" />
            </div>
            <div class="form-group">
                <label for="tipo_evento_id">Tipo Evento: </label>
                <select name="tipo_evento_id">
                    @foreach ($tipoevento as $t)
                        @if ($evento->tipo_evento_id == $t->id)
                            <option value="{{ $t->id }}" selected>
                        @else
                            <option value="{{ $t->id }}">
                        @endif
                            {{ $t->descripcion_tipo_evento }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="lugar_id">Ubicación: </label>
                <select name="lugar_id">
                    @foreach ($lugar as $l)
                        @if ($evento->lugar_id == $l->id)
                            <option value="{{ $l->id }}" selected>
                        @else
                            <option value="{{ $l->id }}">
                        @endif
                            {{ $l->nombre_lugar.' ('.$l->ciudad->nombre_ciudad.' '.$l->ciudad->estado->nombre_estado.')' }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success">Grabar</button>

            </div>

            </form>
        </div>
    </div>
</div>
