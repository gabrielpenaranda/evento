@extends('admin.index')

@section('title', 'Editar Evento')

@section('stylesheets')
    @parent
    <link href="{{ asset('vendor/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/jquery-timepicker/jquery.timepicker.css') }}" rel="stylesheet">
@endsection

@section('content')
    @include('admin.evento._form', ['evento' => $evento, 'lugar' => $lugar, 'tipoevento' => $tipoevento])
@endsection

@section('javascripts')
    @parent
    <script src="{{ asset('vendor/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src=" {{ asset('vendor/jquery-timepicker/jquery.timepicker.js') }} "></script>
    <script src="{{ asset('/vendor/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('input.timepicker').timepicker({
                timeFormat: 'hh:mm p',
                defaultTime: '11',
                minTime: new Date(0,0,0,0,0,0),
                maxTime: new Date(0,0,0,23,55,0),
                interval: 5,
                dynamic: true,
                dropdown: true,
                scrollbar: true
            });
        });
    </script>
    <script type="text/javascript">
    $( function() {
        $( "#datepicker" ).datepicker({
            minDate: +1,
            maxDate: "+3M +15D",
            dateFormat: 'dd/mm/yy',
            dayNamesMin: [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ],
            monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre" ]
         });
    } );
    </script>
@endsection
