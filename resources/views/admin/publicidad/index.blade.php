{{-- PUBLICIDAD --}}

@extends('admin.index')

@section('stylesheets')
    @parent
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-10 text-center">
                <h4>
                    {{ 'Registro de Publicidad' }}
                </h4>
            </div>
            <div class="col-xs-2">
                <a class="btn btn-primary waves-effect waves-light" href="{{ route('admin_publicidad_create') }}">Crear Publicidad</a>
            </div>
            <div class="col-xs-12">
                @if ($publicidad != NULL)
                    <table class="table table-striped">
                        <thead>
                            <th class="text-left">Cliente</th>
                            <th class="text-left">Descripcion</th>
                            <th class="text-left">Fecha Desde/Hasta</th>
                            <th class="text-center">Acciones</th>
                        </thead>
                        <tbody>
                            @foreach ($publicidad as $p)
                                <tr>
                                    <td class="text-left">
                                        {{ $p->cliente->nombre_cliente }}
                                    </td>
                                    <td class="text-left">
                                        {{ $p->descripcion_publicidad }}
                                    </td>
                                    <td class="text-left">
                                        {{ $p->fecha_inicio_publicidad.'/'.$p->fecha_fin_publicidad }}
                                    </td>
                                    <td class="text-center">
                                        @if (Auth::check() && ($p->user_id == Auth::user()->id || $p->user->level == "administrador"))
                                            <form action="{{ route('admin_publicidad_delete', ['publicidad' => $p->id]) }}" method='POST'>
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button class="btn btn-danger waves-effect waves-light" title="Eliminar"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                                                <a class="btn btn-info waves-effect waves-light" href="{{ route('admin_publicidad_edit', ['publicidad' => $p->id]) }}" title="Editar"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                                <a class="btn btn-warning waves-effect waves-light" href="{{ route('admin_publicidad_view', ['publicidad' => $p->id]) }}" title="Ver"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                {!! $publicidad->render() !!}
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

  @section('javascripts')
      @parent
  @endsection
