@extends('admin.index')

@section('title', 'Crear Publicidad')

@section('content')
    @include('admin.publicidad._form', ['publicidad' => $publicidad, 'cliente' => $cliente, 'estado' => $estado, 'pestado' => '$pestado'])
@endsection

@section('stylesheets')
    @parent
    <link href="{{ asset('vendor/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet">
@endsection

@section('javascripts')
    @parent
    <script src="{{ asset('vendor/jquery-ui/jquery-ui.min.js') }}"></script>
    <script type="text/javascript">
    $( function() {
        $( "#datepicker" ).datepicker({
            minDate: +1,
            // maxDate: "+3M +15D",
            dateFormat: 'dd/mm/yy',
            dayNamesMin: [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ],
            monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre" ]
         });
    } );
    $( function() {
        $( "#datepicker2" ).datepicker({
            minDate: +30,
            // maxDate: "+3M +15D",
            dateFormat: 'dd/mm/yy',
            dayNamesMin: [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ],
            monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre" ]
         });
    } );
    </script>
@endsection
