@extends('admin.index')

@section('title', 'Editar Publicidad')

@section('stylesheets')
    @parent
    <link href="{{ asset('vendor/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    @include('admin.publicidad._form', ['publicidad' => $publicidad, 'cliente' => $cliente, 'estado' => $estado, 'pestado' => '$pestado'])
@endsection

@section('javascripts')
    @parent
    <script src="{{ asset('vendor/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('/vendor/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript">
    $( function() {
        $( "#datepicker" ).datepicker({
            minDate: +1,
            maxDate: "+3M +15D",
            dateFormat: 'dd/mm/yy',
            dayNamesMin: [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ],
            monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre" ]
         });
    } );
    </script>
@endsection
