{{-- PUBLICIDAD --}}
<div class="container">
    <div class="row">
        <div class="col-xs-offset-2 col-xs-6">
            @if ($publicidad->exists)
                <h4>Edición de Publicidad</h4>
                <form action="{{ route('admin_publicidad_update', ['publicidad' => $publicidad->id]) }}" method="POST">
                {{ method_field('PUT') }}
            @else
                <h4>Nueva Publicidad</h4>
                <form action="{{ route('admin_publicidad_store') }}" method="POST">
            @endif
        </div>
        <div class="col-xs-2">
            <a class="btn btn-danger waves-effect waves-light" href="{{ route('admin_publicidad') }}">Regresar</a>
        </div>
        <div class="col-xs-offset-2 col-xs-8">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="descripcion_publicidad">Descripción Publicidad:</label>
                <input type="text" name="descripcion_publicidad" class="form-control" value="{{ $publicidad->descripcion_publicidad or old('descripcion_publicidad')}}" />
            </div>
            <div class="form-group">
                <label for="fecha_inicio_publicidad">Fecha Inicio:</label>
                <input type="date" name="fecha_inicio_publicidad" id="datepicker" class="form-control" value="{{ $publicidad->fecha_inicio_publicidad or old('fecha_inicio_publicidad')}}" />
            </div>
            <div class="form-group">
                <label for="fecha_fin_publicidad">Fecha Final:</label>
                <input type="date" name="fecha_fin_publicidad" id="datepicker2" class="form-control" value="{{ $publicidad->fecha_fin_publicidad or old('fecha_fin_publicidad')}}" />
            </div>
            <div class="form-group">
                <label for="cliente_id">Cliente: </label>
                <select name="cliente_id">
                    @foreach ($cliente as $c)
                        @if ($publicidad->cliente_id == $c->id)
                            <option value="{{ $c->id }}" selected>
                        @else
                            <option value="{{ $c->id }}">
                        @endif
                            {{ $c->nombre_cliente.' ('.$c->ciudad->nombre_ciudad.'-'.$c->ciudad->estado->nombre_estado.')' }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="estado_id">Estado: </label>
                <select multiple name="estado_id[]">
                    @foreach ($estado as $e)
                        {{-- @if ($pestado == $e->id)
                            <option value="{{ $l->id }}" selected>
                        @else
                            <option value="{{ $l->id }}">
                        @endif --}}
                        <option value="{{ $e->id }}">
                            {{ $e->nombre_estado }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success">Grabar</button>

            </div>

            </form>
        </div>
    </div>
</div>
