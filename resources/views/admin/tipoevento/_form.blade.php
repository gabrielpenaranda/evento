<div class="container">
    <div class="row">
        <div class="col-xs-offset-2 col-xs-8">
            @if ($tipoevento->exists)
                <h4>Edición de Tipo de Evento</h4>
                <form action="{{ route('admin_tipoevento_update', ['tipoevento' => $tipoevento->id]) }}" method="POST">
                {{ method_field('PUT') }}
            @else
                <h4>Nuevo Tipo de Evento</h4>
                <form action="{{ route('admin_tipoevento_store') }}" method="POST">
            @endif

            {{ csrf_field() }}

            <div class="form-group">
                <label for="descripcion_tipo_evento">Tipo Evento:</label>
                <input type="text" name="descripcion_tipo_evento" class="form-control" value="{{ $tipoevento->descripcion_tipo_evento or old('descripcion_tipo_evento')}}" />
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-success">Grabar</button>
                <a class="btn btn-danger waves-effect waves-light" href="{{ route('admin_tipoevento') }}">Regresar</a>
            </div>

            </form>
        </div>
    </div>
</div>
