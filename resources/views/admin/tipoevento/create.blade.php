@extends('admin.index')

@section('title', 'Crear Tipo de Evento')

@section('stylesheets')
    @parent
@endsection

@section('content')
    @include('admin.tipoevento._form', ['tipoevento' => $tipoevento])
@endsection

@section('javascripts')
    @parent
@endsection
