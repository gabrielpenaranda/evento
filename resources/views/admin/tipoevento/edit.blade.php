@extends('admin.index')

@section('title', 'Editar Tipo de Evento')

@section('stylesheets')
    @parent
@endsection

@section('content')
    @include('admin.tipoevento._form', ['tipoevento' => $tipoevento])
@endsection

@section('javascripts')
    @parent
@endsection
