{{-- TIPO EVENTO --}}

@extends('admin.index')

@section('stylesheets')
    @parent
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-offset-1 col-xs-8 text-center">
                <h4>
                    {{ 'Registro de Tipo de Evento' }}
                </h4>
            </div>
            <div class="col-xs-2">
                <a class="btn btn-primary waves-effect waves-light" href="{{ route('admin_tipoevento_create') }}">Crear Tipo Evento</a>
            </div>
            <div class="col-xs-offset-1 col-xs-10">
                @if ($tipoevento != NULL)
                    <table class="table table-striped">
                        <thead>
                            <th class="text-left">Tipo Evento</th>
                            <th class="text-center">Acciones</th>
                        </thead>
                        <tbody>
                            @foreach ($tipoevento as $te)
                                <tr>
                                    <td class="text-left">
                                    {{ $te->descripcion_tipo_evento }}
                                    </td>
                                    <td class="text-center">
                                        @if (Auth::check() && ($te->user_id == Auth::user()->id || $te->user->level == "administrador"))
                                            <form action="{{ route('admin_tipoevento_delete', ['tipoevento' => $te->id]) }}" method='POST'>
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button class="btn btn-danger waves-effect waves-light" title="Eliminar"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                                                <a class="btn btn-info waves-effect waves-light" href="{{ route('admin_tipoevento_edit', ['tipoevento' => $te->id]) }}" title="Editar"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                {!! $tipoevento->render() !!}
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

  @section('javascripts')
      @parent
  @endsection
