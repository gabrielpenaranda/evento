{{-- PAIS --}}

@extends('admin.index')

@section('stylesheets')
    @parent
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-offset-1 col-xs-8 text-center">
                <h4>
                    {{ 'Registro de Tipo de Lugar' }}
                </h4>
            </div>
            <div class="col-xs-2">
                <a class="btn btn-primary waves-effect waves-light" href="{{ route('admin_tipolugar_create') }}">Crear Tipo Lugar</a>
            </div>
            <div class="col-xs-offset-1 col-xs-10">
                @if ($tipolugar != NULL)
                    <table class="table table-striped">
                        <thead>
                            <th class="text-left">Tipo Lugar</th>
                            <th class="text-center">Acciones</th>
                        </thead>
                        <tbody>
                            @foreach ($tipolugar as $tl)
                                <tr>
                                    <td class="text-left">
                                    {{ $tl->descripcion_tipo_lugar }}
                                    </td>
                                    <td class="text-center">
                                        @if (Auth::check() && ($tl->user_id == Auth::user()->id || $tl->user->level == "administrador"))
                                            <form action="{{ route('admin_tipolugar_delete', ['tipolugar' => $tl->id]) }}" method='POST'>
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button class="btn btn-danger waves-effect waves-light" title="Eliminar"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                                                <a class="btn btn-info waves-effect waves-light" href="{{ route('admin_tipolugar_edit', ['tipolugar' => $tl->id]) }}" title="Editar"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                {!! $tipolugar->render() !!}
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

  @section('javascripts')
      @parent
  @endsection
