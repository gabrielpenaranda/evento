<div class="container">
    <div class="row">
        <div class="col-xs-offset-2 col-xs-8">
            @if ($tipolugar->exists)
                <h4>Edición de Tipo de Lugar</h4>
                <form action="{{ route('admin_tipolugar_update', ['tipolugar' => $tipolugar->id]) }}" method="POST">
                {{ method_field('PUT') }}
            @else
                <h4>Nuevo Tipo de Lugar</h4>
                <form action="{{ route('admin_tipolugar_store') }}" method="POST">
            @endif

            {{ csrf_field() }}

            <div class="form-group">
                <label for="descripcion_tipo_lugar">Tipo Lugar:</label>
                <input type="text" name="descripcion_tipo_lugar" class="form-control" value="{{ $tipolugar->descripcion_tipo_lugar or old('descripcion_tipo_lugar')}}" />
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-success">Grabar</button>
                <a class="btn btn-danger waves-effect waves-light" href="{{ route('admin_tipolugar') }}">Regresar</a>
            </div>

            </form>
        </div>
    </div>
</div>
