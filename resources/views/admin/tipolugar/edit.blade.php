@extends('admin.index')

@section('title', 'Editar Tipo de Lugar')

@section('stylesheets')
    @parent
@endsection

@section('content')
    @include('admin.tipolugar._form', ['tipolugar' => $tipolugar])
@endsection

@section('javascripts')
    @parent
@endsection
