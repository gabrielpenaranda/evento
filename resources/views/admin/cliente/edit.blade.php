@extends('admin.index')

@section('title', 'Editar Cliente')

@section('stylesheets')
    @parent
@endsection

@section('content')
    @include('admin.cliente._form', ['cliente' => $cliente, 'ciudad' => $ciudad])
@endsection

@section('javascripts')
    @parent
@endsection
