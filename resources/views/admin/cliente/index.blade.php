{{-- CLIENTE --}}

@extends('admin.index')

@section('stylesheets')
    @parent
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-10 text-center">
                <h4>
                    {{ 'Registro de Clientes' }}
                </h4>
            </div>
            <div class="col-xs-2">
                <a class="btn btn-primary waves-effect waves-light" href="{{ route('admin_cliente_create') }}">Crear Cliente</a>
            </div>
            <div class="col-xs-12">
                @if ($cliente != NULL)
                    <table class="table table-striped">
                        <thead>
                            <th class="text-left">Cliente</th>
                            <th class="text-left">RIF</th>
                            <th class="text-left">Dirección</th>
                            <th class="text-left">Ubicación</th>
                            <th class="text-center">Acciones</th>
                        </thead>
                        <tbody>
                            @foreach ($cliente as $c)
                                <tr>
                                    <td class="text-left">
                                        {{ $c->nombre_cliente }}
                                    </td>
                                    <td class="text-left">
                                        {{ $c->rif_cliente }}
                                    </td>
                                    <td class="text-left">
                                        {{ $c->direccion_cliente }}
                                    </td>
                                    <td class="text-left">
                                        {{ $c->ciudad->nombre_ciudad.', '.$c->ciudad->estado->nombre_estado }}
                                    </td>
                                    <td class="text-center">
                                        @if (Auth::check() && ($c->user_id == Auth::user()->id || $c->user->level == "administrador"))
                                            <form action="{{ route('admin_cliente_delete', ['cliente' => $c->id]) }}" method='POST'>
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button class="btn btn-danger waves-effect waves-light" title="Eliminar"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                                                <a class="btn btn-info waves-effect waves-light" href="{{ route('admin_cliente_edit', ['cliente' => $c->id]) }}" title="Editar"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                {!! $cliente->render() !!}
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

  @section('javascripts')
      @parent
  @endsection
