{{-- ESTADO --}}
<div class="container">
    <div class="row">
        <div class="col-xs-offset-2 col-xs-8">
            @if ($cliente->exists)
                <h4>Edición de Cliente</h4>
                <form action="{{ route('admin_cliente_update', ['cliente' => $cliente->id]) }}" method="POST">
                {{ method_field('PUT') }}
            @else
                <h4>Nuevo Cliente</h4>
                <form action="{{ route('admin_cliente_store') }}" method="POST">
            @endif

            {{ csrf_field() }}

            <div class="form-group">
                <label for="nombre_cliente">Nombre Cliente:</label>
                <input type="text" name="nombre_cliente" class="form-control" value="{{ $cliente->nombre_cliente or old('nombre_cliente')}}" />
            </div>
            <div class="form-group">
                <label for="rif_cliente">RIF:</label>
                <input type="text" name="rif_cliente" class="form-control" value="{{ $cliente->rif_cliente or old('rif_cliente')}}" />
            </div>
            <div class="form-group">
                <label for="direccion_cliente">Dirección:</label>
                <input type="text" name="direccion_cliente" class="form-control" value="{{ $cliente->direccion_cliente or old('direccion_cliente')}}" />
            </div>
            <div class="form-group">
                <label for="ciudad_id">Ubicación: </label>
                <select name="ciudad_id">
                    @foreach ($ciudad as $c)
                        @if ($cliente->ciudad_id == $c->id)
                            <option value="{{ $c->id }}" selected>
                        @else
                            <option value="{{ $c->id }}">
                        @endif
                            {{ $c->nombre_ciudad.'-'.$c->estado->nombre_estado }}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-success">Grabar</button>
                <a class="btn btn-danger waves-effect waves-light" href="{{ route('admin_cliente') }}">Regresar</a>
            </div>

            </form>
        </div>
    </div>
</div>
