{{-- CLIENTE --}}

@extends('admin.index')

@section('stylesheets')
    @parent
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-10 text-center">
                <h4>
                    {{ 'Registro de Lugares' }}
                </h4>
            </div>
            <div class="col-xs-2">
                <a class="btn btn-primary waves-effect waves-light" href="{{ route('admin_lugar_create') }}">Crear Lugar</a>
            </div>
            <div class="col-xs-12">
                @if ($lugar != NULL)
                    <table class="table table-striped">
                        <thead>
                            <th class="text-left">Lugar</th>
                            <th class="text-left">Tipo Lugar</th>
                            <th class="text-left">Descripción</th>
                            <th class="text-left">Dirección</th>
                            <th class="text-left">Ubicación</th>
                            <th class="text-center">Acciones</th>
                        </thead>
                        <tbody>
                            @foreach ($lugar as $l)
                                <tr>
                                    <td class="text-left">
                                        {{ $l->nombre_lugar }}
                                    </td>
                                    <td class="text-left">
                                        {{ $l->tipo_lugar->descripcion_tipo_lugar }}
                                    </td>
                                    <td class="text-left">
                                        {{ $l->descripcion_lugar }}
                                    </td>
                                    <td class="text-left">
                                        {{ $l->direccion_lugar }}
                                    </td>
                                    <td class="text-left">
                                        {{ $l->ciudad->nombre_ciudad.', '.$l->ciudad->estado->nombre_estado }}
                                    </td>
                                    <td class="text-center">
                                        @if (Auth::check() && ($l->user_id == Auth::user()->id || $l->user->level == "administrador"))
                                            <form action="{{ route('admin_lugar_delete', ['lugar' => $l->id]) }}" method='POST'>
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button class="btn btn-danger waves-effect waves-light" title="Eliminar"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                                                <a class="btn btn-info waves-effect waves-light" href="{{ route('admin_lugar_edit', ['lugar' => $l->id]) }}" title="Editar"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                {!! $lugar->render() !!}
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

  @section('javascripts')
      @parent
  @endsection
