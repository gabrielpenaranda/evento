{{-- ESTADO --}}
<div class="container">
    <div class="row">
        <div class="col-xs-offset-2 col-xs-8">
            @if ($lugar->exists)
                <h4>Edición de Lugar</h4>
                <form action="{{ route('admin_lugar_update', ['lugar' => $lugar->id]) }}" method="POST">
                {{ method_field('PUT') }}
            @else
                <h4>Nuevo Lugar</h4>
                <form action="{{ route('admin_lugar_store') }}" method="POST">
            @endif
            {{ csrf_field() }}
            <div class="form-group">
                <label for="nombre_lugar">Nombre Lugar:</label>
                <input type="text" name="nombre_lugar" class="form-control" value="{{ $lugar->nombre_lugar or old('nombre_lugar')}}" />
            </div>
            <div class="form-group">
                <label for="descripcion_lugar">Descripción:</label>
                <input type="text" name="descripcion_lugar" class="form-control" value="{{ $lugar->descripcion_lugar or old('descripcion_lugar')}}" />
            </div>
            <div class="form-group">
                <label for="direccion_lugar">Dirección:</label>
                <input type="text" name="direccion_lugar" class="form-control" value="{{ $lugar->direccion_lugar or old('direccion_lugar')}}" />
            </div>
            <div class="form-group">
                <label for="ciudad_id">Ubicación: </label>
                <select name="ciudad_id">
                    @foreach ($ciudad as $c)
                        @if ($lugar->ciudad_id == $c->id)
                            <option value="{{ $c->id }}" selected>
                        @else
                            <option value="{{ $c->id }}">
                        @endif
                            {{ $c->nombre_ciudad.'-'.$c->estado->nombre_estado }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="tipo_lugar_id">Tipo Lugar: </label>
                <select name="tipo_lugar_id">
                    @foreach ($tipolugar as $t)
                        @if ($lugar->tipo_lugar_id == $t->id)
                            <option value="{{ $t->id }}" selected>
                        @else
                            <option value="{{ $t->id }}">
                        @endif
                            {{ $t->descripcion_tipo_lugar }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success">Grabar</button>
                <a class="btn btn-danger waves-effect waves-light" href="{{ route('admin_lugar') }}">Regresar</a>
            </div>

            </form>
        </div>
    </div>
</div>
