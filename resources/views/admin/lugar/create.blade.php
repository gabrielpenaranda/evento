@extends('admin.index')

@section('title', 'Crear Lugar')

@section('stylesheets')
    @parent
@endsection

@section('content')
    @include('admin.lugar._form', ['lugar' => $lugar, 'ciudad' => $ciudad, 'tipolugar' => $tipolugar])
@endsection

@section('javascripts')
    @parent
@endsection
