<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEventosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_evento');
            $table->text('descripcion_evento');
            $table->integer('lugar_id')->unsigned();

            $table->foreign('lugar_id')->references('id')->on('lugares')->onDelete('cascade');

            $table->integer('tipo_evento_id')->unsigned();

            $table->foreign('tipo_evento_id')->references('id')->on('tipo_eventos');

            $table->date('fecha_evento');
            $table->time('hora_evento');
            $table->string('imagen_evento');
            $table->integer('user_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eventos');
    }
}
