<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLugaresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lugares', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_lugar');
            $table->string('descripcion_lugar');
            $table->string('direccion_lugar');
            $table->integer('ciudad_id')->unsigned();

            $table->foreign('ciudad_id')->references('id')->on('ciudades')->onDelete('cascade');

            $table->string('imagen_lugar');
            $table->integer('tipo_lugar_id')->unsigned();

            $table->foreign('tipo_lugar_id')->references('id')->on('tipo_lugares');

            $table->integer('user_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lugares');
    }
}
